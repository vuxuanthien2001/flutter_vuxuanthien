import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:flutter_reaction_button/flutter_reaction_button.dart';
import 'package:flutter_thienx/assets/db_icons.dart';

import 'Person.dart';

// ignore: must_be_immutable
class MyDetail extends StatelessWidget {
  List<Person> listPerson = [
    Person(
        "@id1",
        "Andrei",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOAXDHKd1hwIIr3H7sq2XOCrRzmzbUecMJuQ&usqp=CAU",
        1),
    Person(
        "@id2",
        "Zoro",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
        1),
    Person(
        "@id3",
        "Cade",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU",
        1),
    Person(
        "@id4",
        "Jin",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0tfaK8Jt3JuyCLwZx44SW3fFNR-sWA6s2mQ&usqp=CAU",
        0),
    Person(
        "@id5",
        "Bim Bim",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU",
        0)
  ];
  MyDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Expanded(
          child: Column(
            children: [
              _information(),
              _loadImage(),
              _status(),
              _posts(),
              _comment()
            ],
          ),
        ),
      ),
    );
  }

  // ignore: unused_element
  Widget _information() {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        Row(
          children: [
            const SizedBox(
              width: 10,
            ),
            SizedBox(
              height: 40,
              width: 40,
              child: CircleAvatar(
                  backgroundImage: NetworkImage(listPerson[0].image)),
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      Text(listPerson[0].name,
                          maxLines: 1, overflow: TextOverflow.ellipsis),
                      Text(
                        listPerson[0].id,
                        style: const TextStyle(color: Colors.grey),
                      )
                    ],
                  )),
            ),
            Container(
              margin: const EdgeInsets.all(25),
              child: const Text("12 min", style: TextStyle(color: Colors.grey)),
            )
          ],
        ),
      ],
    );
  }

  Widget _loadImage() {
    return Image.network(
      "https://images5.alphacoders.com/481/thumbbig-481903.webp",
      width: double.infinity,
      fit: BoxFit.fitWidth,
    );
  }

  Widget _status() {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Icon(Icons.favorite_border_outlined),
            const SizedBox(
              width: 5,
            ),
            const SizedBox(
              width: 5,
            ),
            const Icon(Icons.chat_bubble_outline),
            const SizedBox(
              width: 5,
            ),
            const SizedBox(
              width: 5,
            ),
            const Icon(DBIcon.forward),
            const SizedBox(
              width: 140,
            ),
            const Text(
              "7,890 views",
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }

  Widget _posts() {
    return Expanded(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      // ignore: prefer_const_literals_to_create_immutables
      children: [
        const Text(
            "- Free is Ipsum 15th th variations a aiteration attributed suffered with 15th. There even by there century graphic"),
        const Text.rich(
          TextSpan(
            children: [
              TextSpan(
                text: '- Have of designs true ',
              ),
              WidgetSpan(child: Icon(DBIcon.grin_squint_tears)),
              TextSpan(
                text: ' are Ípum De out are ',
              ),
              WidgetSpan(
                child: Icon(DBIcon.kiss_wink_heart),
              ),
              TextSpan(
                text: ' are ',
              ),
              WidgetSpan(child: Icon(DBIcon.laugh_squint)),
              TextSpan(
                text: ' Internet predefined alteration Bonorum free specimen',
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        const Text("View all 28 comments", style: TextStyle(color: Colors.grey))
      ],
    ));
  }

  // ignore: unused_element
  Widget _comment() {
    return SizedBox(
      child: Row(
        children: [
          Container(
            height: 45,
            margin: const EdgeInsets.only(bottom: 150),
            width: 270,
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 214, 207, 207),
              borderRadius: BorderRadius.circular(15),
            ),
            child: const TextField(
              decoration: InputDecoration(
                hintText: " Comment me",
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 150),
            child: const Text(
              " Post",
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }
}
