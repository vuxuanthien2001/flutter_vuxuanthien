import 'package:flutter/material.dart';

import 'Person.dart';

// ignore: must_be_immutable
class MyConnectPage extends StatelessWidget {
  List<Person> listPerson = [
    Person(
        "@id1",
        "Andrei",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOAXDHKd1hwIIr3H7sq2XOCrRzmzbUecMJuQ&usqp=CAU",
        1),
    Person(
        "@id2",
        "Zoro",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
        1),
    Person(
        "@id3",
        "Cade",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU",
        1),
    Person(
        "@id4",
        "Jin",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0tfaK8Jt3JuyCLwZx44SW3fFNR-sWA6s2mQ&usqp=CAU",
        0),
    Person(
        "@id5",
        "Bim Bim",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU",
        0)
  ];

  List<String> images = [
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLcLrRTU-Y3JFpxlNtEyzUzDvrG8HLMomhwA&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAm0_gOTq0-Xp24EVWDyrq6V3RQTMTD7etdA&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcST0YLCNt8bda-AX3Q4eRK-fJ5Auz0DHCXYCQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSN9S64AFs7lDdkIujru9ROy-l3Oe23IVT9Q&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXBoHZP3TclTXMkpNsMibFx-krl3EYsJ2b8Q&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTRGeR3YMIIu2DlSUwZRb8jpDGsCpkkxvybAQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0X4IS0e2WbFd-hLkeUbNAuleXNIHl5ekA_w&usqp=CAU"
  ];
  MyConnectPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: const EdgeInsets.only(top: 60, left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _back(),
              _information(),
              _connect_Follow(context),
              const Text(
                "Posts",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              _gridImage()
            ],
          )),
    );
  }

  Widget _back() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: Container(
          alignment: Alignment.topLeft,
          child: const Icon(
            Icons.chevron_left,
            size: 30,
            color: Colors.black,
          )),
    );
  }

  Widget _information() {
    return Row(
      children: [
        CircleAvatar(
          radius: 40,
          backgroundImage: NetworkImage(listPerson[0].image),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                const Text(
                  "Jedd Kouma",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                const Text(
                  'April 12th',
                  style: TextStyle(fontSize: 15, color: Colors.grey),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _connect_Follow(var context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 1,
                height: 40,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: ElevatedButton(
                    onPressed: () {},
                    child: const Text(
                      "Connect",
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '105k',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.black.withOpacity(1)),
                  ),
                  const Text(
                    'Fllowers',
                    style: TextStyle(fontSize: 15, color: Colors.grey),
                  )
                ],
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '45',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.black.withOpacity(1)),
                  ),
                  const Text(
                    'Following',
                    style: TextStyle(fontSize: 15, color: Colors.grey),
                  )
                ],
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _gridImage() {
    return Expanded(
        child: GridView.builder(
      itemCount: images.length,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, crossAxisSpacing: 4.0, mainAxisSpacing: 4.0),
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              image: DecorationImage(image: NetworkImage(images[index]))),
        );
      },
    ));
  }

  Widget_posts(){
    
  }
}
