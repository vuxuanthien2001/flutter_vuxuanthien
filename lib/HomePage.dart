// ignore_for_file: use_key_in_widget_constructors, unused_element

import 'package:flutter/material.dart';

import 'DetailPage.dart';
import 'DetailPersonPage.dart';
import 'NewsPage.dart';
import 'Person.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MyHomePageState();
  }
}

class MyHomePageState extends State<MyHomePage> {
  int selectedIndex = 0;
  final Widget _myHome = MyHome();
  final Widget _mySearch = MySearch();
  final Widget _myFlow = MyFlow();
  final Widget _myProfile = MyProfile();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getBody(),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: selectedIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '_',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: '_',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.timeline_outlined),
            label: '_',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: '_',
          ),
        ],
        onTap: (int index) {
          onTapHandler(index);
        },
      ),
    );
  }

  Widget getBody() {
    if (selectedIndex == 0) {
      return _myHome;
    } else if (selectedIndex == 1) {
      return _mySearch;
    } else if (selectedIndex == 2) {
      return _myFlow;
    } else {
      return _myProfile;
    }
  }

  void onTapHandler(int index) {
    setState(() {
      selectedIndex = index;
    });
  }
}

// ignore: must_be_immutable
class MyHome extends StatelessWidget {
  List<Person> listPerson = [
    Person(
        "@id1",
        "Andrei",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOAXDHKd1hwIIr3H7sq2XOCrRzmzbUecMJuQ&usqp=CAU",
        0),
    Person(
        "@id2",
        "Zoro",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
        1),
    Person(
        "@id3",
        "Cade",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU",
        1),
    Person(
        "@id4",
        "Jin",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0tfaK8Jt3JuyCLwZx44SW3fFNR-sWA6s2mQ&usqp=CAU",
        0),
    Person(
        "@id5",
        "Bim Bim",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU",
        0)
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [_myStatus(), _showListPost()],
    );
  }

  Widget _myStatus() {
    return Column(
      children: [
        const SizedBox(
          height: 25,
        ),
        SizedBox(
          height: 80,
          child: Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: listPerson.length,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Stack(
                              children: [
                                if (listPerson[index].status == 1)
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => MyNewsPage(),
                                        ),
                                      );
                                    },
                                    child: CircleAvatar(
                                      radius: 28,
                                      backgroundColor: Colors.red,
                                      child: CircleAvatar(
                                          radius: 25,
                                          backgroundImage: NetworkImage(
                                              listPerson[index].image)),
                                    ),
                                  ),
                                if (listPerson[index].status == 0)
                                  CircleAvatar(
                                      radius: 25,
                                      backgroundImage: NetworkImage(
                                          listPerson[index].image)),
                                if (index == 0)
                                  Positioned(
                                    right: 0,
                                    bottom: 0,
                                    child: Container(
                                      height: 16,
                                      width: 16,
                                      decoration: const BoxDecoration(
                                          image: DecorationImage(
                                              image: NetworkImage(
                                                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQemECSd29pBylUjFEMIfYkoyXUr8rm-v2Br2f1PYB7pfysdN8dE2wIws46aAK2InzAIkc&usqp=CAU")),
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                if (index == 1)
                                  Positioned(
                                    left: 30,
                                    bottom: 40,
                                    child: Container(
                                      height: 16,
                                      width: 30,
                                      decoration: const BoxDecoration(
                                          image: DecorationImage(
                                              image: NetworkImage(
                                                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH_noFAFoiKRb84NpndzbjVDglHxG31eoOgQ&usqp=CAU")),
                                          shape: BoxShape.circle),
                                    ),
                                  )
                              ],
                            ))
                      ],
                    );
                  })),
        ),
      ],
    );
  }

  Widget _information(var context, var index) {
    return Row(
      children: [
        const SizedBox(
          width: 10,
        ),
        SizedBox(
          height: 40,
          width: 40,
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => MyDetailPersonPage(),
                ),
              );
            },
            child: CircleAvatar(
                backgroundImage: NetworkImage(listPerson[index].image)),
          ),
        ),
        Expanded(
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  Text(listPerson[index].name,
                      maxLines: 1, overflow: TextOverflow.ellipsis),
                  Text(
                    listPerson[index].id,
                    style: const TextStyle(color: Colors.grey),
                  )
                ],
              )),
        ),
        Container(
          margin: const EdgeInsets.all(25),
          child: const Text(
            "12 min",
            style: TextStyle(color: Colors.grey),
          ),
        )
      ],
    );
  }

  Widget _showImage(var context, var index) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MyDetail(),
          ),
        );
      },
      child: Image.network(
        listPerson[index].image,
        width: double.maxFinite,
        fit: BoxFit.fitWidth,
      ),
    );
  }

  Widget _showPersonWatched() {
    return Column(
      children: [
        const SizedBox(height: 10,),
        Row(
          children: [
            const SizedBox(
              width: 20,
            ),
            Stack(
              children: [
                SizedBox(
                  width: 30,
                  height: 30,
                  child: CircleAvatar(
                      backgroundImage: NetworkImage(listPerson[3].image)),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 25),
                  width: 30,
                  height: 30,
                  child: CircleAvatar(
                      backgroundImage: NetworkImage(listPerson[4].image)),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 50),
                  width: 30,
                  height: 30,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU"))),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(left: 10),
              child: const Text("24 like"),
            ),
            Expanded(
                child: Container(
              alignment: Alignment.centerRight,
              child: const Icon(Icons.more_vert),
            )),
            const SizedBox(
              width: 20,
            ),
          ],
        ),
      ],
    );
  }

  Widget _showComment() {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          children: const [
            SizedBox(
              width: 20,
            ),
            Text(
              "Nola padilla",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              "Humour, it use used middle",
            )
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          children: const [
            SizedBox(
              width: 20,
            ),
            Text(
              "Kristie Smith",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              "Sentence, free chunks is in",
            )
          ],
        ),
      ],
    );
  }

  Widget _showListPost() {
    return Expanded(
        child: ListView.builder(
      itemCount: listPerson.length,
      itemBuilder: (context, index) {
        return Column(
          children: [
            _information(context, index),
            _showImage(context, index),
            _showPersonWatched(),
            _showComment()
          ],
        );
      },
    ));
  }
}

// ignore: must_be_immutable
class MySearch extends StatelessWidget {
  List<Person> listPerson = [
    Person(
        "@id1",
        "Andrei",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOAXDHKd1hwIIr3H7sq2XOCrRzmzbUecMJuQ&usqp=CAU",
        1),
    Person(
        "@id2",
        "Zoro",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
        1),
    Person(
        "@id3",
        "Cade",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU",
        1),
    Person(
        "@id4",
        "Jin",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0tfaK8Jt3JuyCLwZx44SW3fFNR-sWA6s2mQ&usqp=CAU",
        0),
    Person(
        "@id5",
        "Bim Bim",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU",
        0)
  ];

  MySearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [_search(), _recent(), _result(), _showListResult()],
      ),
    );
  }

  Widget _search() {
    return Column(
      // ignore: prefer_const_literals_to_create_immutables
      children: [
        const SizedBox(
          height: 20,
        ),
        const SizedBox(
          height: 50,
          child: TextField(
            decoration: InputDecoration(
              hintText: "Search messgages",
              filled: true,
              prefixIcon: Icon(Icons.search),
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _recent() {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          children: const [
            Text(
              "Recent",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 240,
            ),
            Text(
              "Clear",
              style: TextStyle(color: Colors.blue),
            ),
          ],
        ),
        SizedBox(
          height: 100,
          child: Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: listPerson.length,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Container(
                            margin: const EdgeInsets.only(right: 15.0),
                            child: Stack(
                              children: [
                                CircleAvatar(
                                    backgroundImage:
                                        NetworkImage(listPerson[index].image)),
                                if (listPerson[index].status == 1)
                                  Positioned(
                                    right: -5,
                                    bottom: 0,
                                    child: Container(
                                      height: 16,
                                      width: 25,
                                      decoration: const BoxDecoration(
                                          color:
                                              Color.fromARGB(255, 7, 255, 48),
                                          shape: BoxShape.circle),
                                    ),
                                  )
                              ],
                            ))
                      ],
                    );
                  })),
        ),
      ],
    );
  }

  Widget _result() {
    return Column(
      children: [
        Row(
          children: const [
            Text(
              "Result",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ],
    );
  }

  Widget _showImage(var index) {
    return SizedBox(
      height: 40,
      width: 40,
      child:
          CircleAvatar(backgroundImage: NetworkImage(listPerson[index].image)),
    );
  }

  Widget _showInformation(var index) {
    return Expanded(
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              Text(listPerson[index].name,
                  maxLines: 1, overflow: TextOverflow.ellipsis),
              Text(
                listPerson[index].id,
                style: const TextStyle(color: Colors.grey),
              )
            ],
          )),
    );
  }

  Widget _showListResult() {
    return Expanded(
        child: ListView.builder(
            itemCount: listPerson.length,
            itemBuilder: (context, index) {
              return Row(
                children: [
                  _showImage(index),
                  _showInformation(index),
                  SizedBox(
                    width: 80,
                    child: ElevatedButton(
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                  side: const BorderSide(color: Colors.blue)))),
                      onPressed: () {},
                      child: const Text('Follow'),
                    ),
                  ),
                ],
              );
            }));
  }
}

// ignore: must_be_immutable
class MyFlow extends StatelessWidget {
  List<Person> listPerson = [
    Person(
        "@id1",
        "Andrei",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOAXDHKd1hwIIr3H7sq2XOCrRzmzbUecMJuQ&usqp=CAU",
        1),
    Person(
        "@id2",
        "Zoro",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
        1),
    Person(
        "@id3",
        "Cade",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU",
        1),
    Person(
        "@id4",
        "Jin",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0tfaK8Jt3JuyCLwZx44SW3fFNR-sWA6s2mQ&usqp=CAU",
        0),
    Person(
        "@id5",
        "Bim Bim",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU",
        0)
  ];

  MyFlow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          _followRequest(),
          _today(),
          _recent(),
          _suggestion(),
          _showListPerson()
        ],
      ),
    );
  }

  Widget _followRequest() {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        Row(
          children: [
            const SizedBox(
              height: 50,
              width: 50,
              child: CircleAvatar(
                radius: 50,
                backgroundColor: Colors.black,
                child: CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.white,
                  child: Icon(
                    Icons.person_add,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const Text("Follow request",
                          maxLines: 1, overflow: TextOverflow.ellipsis),
                      const Text(
                        "Approve or ignore request",
                        style: TextStyle(color: Colors.grey),
                      )
                    ],
                  )),
            ),
          ],
        ),
      ],
    );
  }

  Widget _today() {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          children: const [
            Text(
              "Today",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Stack(
              children: [
                Container(
                  width: 30,
                  height: 30,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLcLrRTU-Y3JFpxlNtEyzUzDvrG8HLMomhwA&usqp=CAU"))),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 25),
                  width: 40,
                  height: 40,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU"))),
                ),
              ],
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      // ignore: prefer_const_constructors
                      Text.rich(const TextSpan(
                          text: "anami, bucchanlan",
                          style: TextStyle(fontWeight: FontWeight.bold),
                          children: <InlineSpan>[
                            TextSpan(
                                text: " and",
                                style: TextStyle(color: Colors.grey)),
                            TextSpan(
                              text: " 5 orther",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            )
                          ])),
                      const Text(
                        "started following you. 2d",
                        style: TextStyle(color: Colors.grey),
                      )
                    ],
                  )),
            ),
          ],
        ),
      ],
    );
  }

  Widget _recent() {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          children: const [
            Text(
              "Recent",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          children: [
            SizedBox(
              height: 40,
              width: 40,
              child: CircleAvatar(
                  backgroundImage: NetworkImage(listPerson[3].image)),
            ),
            const SizedBox(
              width: 10,
            ),
            const Expanded(
              child: Text.rich(TextSpan(text: "Follow", children: <InlineSpan>[
                TextSpan(
                    text: " Carty, Reily",
                    style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(
                  text: " and orther you no to see their photos and video.",
                ),
                TextSpan(
                  text: " 5w",
                  style: TextStyle(color: Colors.grey),
                ),
              ])),
            ),
            const SizedBox(
              height: 5,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            SizedBox(
              height: 40,
              width: 40,
              child: CircleAvatar(
                  backgroundImage: NetworkImage(listPerson[4].image)),
            ),
            const SizedBox(
              width: 10,
            ),
            const Expanded(
              child: Text.rich(TextSpan(
                  text: "Irfan Alvarado, Halima Snider",
                  style: TextStyle(fontWeight: FontWeight.bold),
                  children: <InlineSpan>[
                    TextSpan(
                        text: " on Social. See their post ",
                        style: TextStyle(color: Colors.grey)),
                    TextSpan(
                      text: " 10w",
                      style: TextStyle(color: Colors.grey),
                    ),
                  ])),
            ),
            const SizedBox(
              height: 5,
            ),
          ],
        ),
      ],
    );
  }

  Widget _showImge(var index) {
    return SizedBox(
      height: 40,
      width: 40,
      child:
          CircleAvatar(backgroundImage: NetworkImage(listPerson[index].image)),
    );
  }

  Widget _showInformation(var index) {
    return Expanded(
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              Text(listPerson[index].name,
                  maxLines: 1, overflow: TextOverflow.ellipsis),
              Text(
                listPerson[index].id,
                style: const TextStyle(color: Colors.grey),
              )
            ],
          )),
    );
  }

  Widget _orther() {
    return Row(
      children: [
        SizedBox(
          width: 80,
          child: ElevatedButton(
            style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        side: const BorderSide(color: Colors.blue)))),
            onPressed: () {},
            child: const Text('Follow'),
          ),
        ),
        const SizedBox(
          width: 5,
        ),
        const Icon(
          Icons.close,
          size: 15,
        )
      ],
    );
  }

  Widget _suggestion() {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          children: const [
            Text(
              "Suggestion",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ],
    );
  }

  Widget _showListPerson() {
    return Expanded(
        child: ListView.builder(
            itemCount: listPerson.length,
            itemBuilder: (context, index) {
              return Row(
                children: [
                  _showImge(index),
                  _showInformation(index),
                  _orther()
                ],
              );
            }));
  }
}

// ignore: must_be_immutable
class MyProfile extends StatelessWidget {
  List<Person> listPerson = [
    Person(
        "@id1",
        "Andrei",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOAXDHKd1hwIIr3H7sq2XOCrRzmzbUecMJuQ&usqp=CAU",
        1),
    Person(
        "@id2",
        "Zoro",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
        1),
    Person(
        "@id3",
        "Cade",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU",
        1),
    Person(
        "@id4",
        "Jin",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0tfaK8Jt3JuyCLwZx44SW3fFNR-sWA6s2mQ&usqp=CAU",
        0),
    Person(
        "@id5",
        "Bim Bim",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU",
        0)
  ];

  MyProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _information(),
          _SeemoreEdit(),
          _parameter(),
          _complete(),
          _listProfile()
        ],
      ),
    );
  }

  Widget _information() {
    return Column(
      children: [
        const SizedBox(
          height: 24,
        ),
        SizedBox(
          height: 100,
          width: 100,
          child:
              CircleAvatar(backgroundImage: NetworkImage(listPerson[0].image)),
        ),
        const SizedBox(
          height: 10,
        ),
        const Text(
          "Martyn Rankin",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        const Text(
          "Fashion model",
          style: TextStyle(color: Colors.grey),
        ),
        const Text(
          "I love to be kind",
          style: TextStyle(color: Colors.grey),
        ),
        const Text(
          "From heaven",
          style: TextStyle(color: Colors.grey),
        ),
      ],
    );
  }

  // ignore: non_constant_identifier_names
  Widget _SeemoreEdit() {
    return Row(
      children: [
        const Text(
          "See more",
          style: TextStyle(
              decoration: TextDecoration.underline, color: Colors.black),
        ),
        const SizedBox(
          width: 180,
        ),
        SizedBox(
          width: 80,
          child: ElevatedButton(
            child: const Text("Edit"),
            onPressed: () {},
          ),
        ),
      ],
    );
  }

  Widget _parameter() {
    return Card(
      child: Row(children: [
        const SizedBox(
          width: 10,
        ),
        Column(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const SizedBox(
              height: 10,
            ),
            const Text("15",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
            const SizedBox(
              height: 10,
            ),
            const Text("Post",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
        const SizedBox(
          width: 60,
        ),
        Column(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const SizedBox(
              height: 10,
            ),
            const Text("486",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
            const SizedBox(
              height: 10,
            ),
            const Text("Followers",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
        const SizedBox(
          width: 60,
        ),
        Column(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const SizedBox(
              height: 10,
            ),
            const Text("58",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
            const SizedBox(
              height: 10,
            ),
            const Text("Following",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ]),
    );
  }

  Widget _complete() {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          children: const [
            Text(
              "Complete your profile",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          children: const [
            Text(
              "2 OF 4",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 59, 255, 118),
                  fontSize: 12),
            ),
            Text(
              " COMPLETE",
              style: TextStyle(color: Colors.grey),
            )
          ],
        ),
      ],
    );
  }

  Widget _listProfile() {
    return Expanded(
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: listPerson.length,
            itemBuilder: (context, index) {
              return Row(
                children: [
                  SizedBox(
                    height: 220,
                    child: Card(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 20,
                          ),
                          const SizedBox(
                            height: 50,
                            width: 200,
                            child: CircleAvatar(
                              radius: 50,
                              backgroundColor: Colors.black,
                              child: CircleAvatar(
                                radius: 40,
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.person,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Text("Add profile photo",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16)),
                          const SizedBox(
                            height: 5,
                          ),
                          const Text("15th available always a first",
                              style: TextStyle(color: Colors.grey)),
                          const SizedBox(
                            height: 3,
                          ),
                          const Text("text look for",
                              style: TextStyle(color: Colors.grey)),
                          const SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            width: 80,
                            child: ElevatedButton(
                              child: const Text("Edit"),
                              onPressed: () {},
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            }));
  }
}
