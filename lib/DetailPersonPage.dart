import 'package:flutter/material.dart';

import 'ConnectPage.dart';
import 'Person.dart';

// ignore: must_be_immutable
class MyDetailPersonPage extends StatelessWidget {
  List<Person> listPerson = [
    Person(
        "@id1",
        "Andrei",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOAXDHKd1hwIIr3H7sq2XOCrRzmzbUecMJuQ&usqp=CAU",
        1),
    Person(
        "@id2",
        "Zoro",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
        1),
    Person(
        "@id3",
        "Cade",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU",
        1),
    Person(
        "@id4",
        "Jin",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0tfaK8Jt3JuyCLwZx44SW3fFNR-sWA6s2mQ&usqp=CAU",
        0),
    Person(
        "@id5",
        "Bim Bim",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU",
        0)
  ];
  MyDetailPersonPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(listPerson[0].image), fit: BoxFit.cover),
        ),
        child: Container(
          padding: const EdgeInsets.only(top: 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _back(),
              Container(
                padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: MediaQuery.of(context).size.height / 2,
                    bottom: 40),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: FractionalOffset.bottomCenter,
                    end: FractionalOffset.topCenter,
                    colors: [
                      Colors.black.withOpacity(0.6),
                      Colors.grey.withOpacity(0.3),
                      Colors.grey.withOpacity(0.0),
                    ],
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Zayn Molloy',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 30),
                    ),
                    const Text(
                      'Fashion Model',
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [_post(), _followers(), _following()],
                    ),
                    _follow(context),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _back() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 20),
        child: Container(
            alignment: Alignment.topLeft,
            child: const Icon(
              Icons.chevron_left,
              size: 30,
            )),
      ),
    );
  }

  Widget _post() {
    return Expanded(
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          const Text(
            '15',
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          const Text(
            'Posts',
            style: TextStyle(
                fontSize: 18, color: Color.fromARGB(255, 230, 216, 216)),
          )
        ],
      ),
    );
  }

  Widget _followers() {
    return Expanded(
      child: Container(
        decoration: const BoxDecoration(
          border: Border(
            right: BorderSide(color: Colors.white),
            left: BorderSide(color: Colors.white),
          ),
        ),
        child: Column(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Text(
              '45',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            const Text(
              'Fllowers',
              style: TextStyle(
                  fontSize: 18, color: Color.fromARGB(255, 230, 216, 216)),
            )
          ],
        ),
      ),
    );
  }

  Widget _following() {
    return Expanded(
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          const Text(
            '58',
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          const Text(
            'Following',
            style: TextStyle(
                fontSize: 18, color: Color.fromARGB(255, 230, 216, 216)),
          )
        ],
      ),
    );
  }

  Widget _follow(var context) {
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 15),
            child: SizedBox(
              height: 40,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25),
                child: ElevatedButton(
                  onPressed: () {},
                  child: const Text(
                    "Follow",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 12),
          child: Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 175, 175, 175),
              borderRadius: BorderRadius.circular(25),
            ),
            child: IconButton(
              icon: const Icon(Icons.trending_flat_outlined),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyConnectPage(),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
