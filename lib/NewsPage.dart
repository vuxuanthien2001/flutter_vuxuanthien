import 'package:flutter/material.dart';

import 'Person.dart';

// ignore: must_be_immutable
class MyNewsPage extends StatelessWidget {
  List<Person> listPerson = [
    Person(
        "@id1",
        "Andrei",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOAXDHKd1hwIIr3H7sq2XOCrRzmzbUecMJuQ&usqp=CAU",
        1),
    Person(
        "@id2",
        "Zoro",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
        1),
    Person(
        "@id3",
        "Cade",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU",
        1),
    Person(
        "@id4",
        "Jin",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0tfaK8Jt3JuyCLwZx44SW3fFNR-sWA6s2mQ&usqp=CAU",
        0),
    Person(
        "@id5",
        "Bim Bim",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU",
        0)
  ];
  MyNewsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: const EdgeInsets.only(top: 50, left: 10, right: 10),
      child: Column(
        children: [
          _process(),
          _information(),
          _loadImage(),
          _sendMessage()
        ],
      ),
    ));
  }

  Widget _process() {
    return const LinearProgressIndicator(
      value: 0.3,
      backgroundColor: Colors.black26,
      valueColor: AlwaysStoppedAnimation<Color>(
        Colors.grey,
        //<-- SEE HERE
      ),
    );
  }

  Widget _information() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        children: [
          CircleAvatar(
            radius: 20,
            backgroundImage: NetworkImage(listPerson[0].image),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: listPerson[0].name,
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        const WidgetSpan(
                          child: Padding(
                            padding: EdgeInsets.only(left: 10),
                          ),
                        ),
                        const TextSpan(
                          text: '10 min',
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                  const Text(
                    '106 views',
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(right: 20),
            child: Icon(
              Icons.more_vert,
              color: Color.fromARGB(255, 105, 105, 105),
            ),
          ),
        ],
      ),
    );
  }

  Widget _loadImage() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Image.network(
          listPerson[0].image,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _sendMessage() {
    return Row(
      children: const [
        Expanded(
          child: TextField(
            decoration: InputDecoration(
              hintText: "Send message",
              border: InputBorder.none,
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Icon(Icons.send),
      ],
    );
  }
}
