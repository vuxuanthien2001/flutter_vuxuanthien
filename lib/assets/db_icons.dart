import 'package:flutter/widgets.dart';

class DBIcon {
  DBIcon._();

  static const _kFontFam = 'DBIcon';

  static const IconData forward = IconData(0xe800, fontFamily: _kFontFam);
  static const IconData grin_squint_tears = IconData(0xf586, fontFamily: _kFontFam);
  static const IconData kiss_wink_heart = IconData(0xf598, fontFamily: _kFontFam);
  static const IconData laugh_squint = IconData(0xf59b, fontFamily: _kFontFam);
}
