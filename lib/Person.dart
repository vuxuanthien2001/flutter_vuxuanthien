class Person {
  var id;
  var name;
  var image;
  var status;

  Person(var id, var name, var image, var status) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.status = status;
  }
}